package webserver;

import java.nio.file.FileSystemNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Single file server
 *
 * @author Andrei Palade <andrey.palade at googlemail.com>
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        if (args.length != 2) {
            System.out.println("Usage: java Main file");
        }
        else {
            
            try {
                String path = args[0];
                int port = Integer.parseInt(args[1]);
                
                SingleFileHTTPServer server = new SingleFileHTTPServer(path, port);
                server.start();
                
            } catch (IllegalArgumentException | FileSystemNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
