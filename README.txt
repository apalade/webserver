How does your system work?
Starts a server socket on a hardcoded port number (i.e., 8080), reads the request from the user. Opens the file passed as an argument and retrieves the required line. Builds the response by adding the http header template and the data, and returns the result to the user. To serve requests, I used a fixed pool of threads of size 100. 

What do we need to build your system?
- Have java (jdk v1.8) and ant (v1.9.6) installed and available on command line (add to PATH variable)
- run ./build.sh
 
How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?
Needs to be evaluated, however the available implementation is minimal. I need to investigate this, but an option is to pre-process the file and keep track the size of each line. Then use skip lists to avoid going line by line. Once you know where the line in the file is, you can go at that address in memory to retrieve the line without iterating through each line. 

How will your system perform with 100 users? 10000 users? 1000000 users?
- Results for 10 runs are in the Excel file in results/results.xlsx
- Tool used to run the performance test: Apache Benchmark
- Experiment design: changing number of users (100, 1000, 10000, 100000, 1000000)
- Metrics: number of requests per second (Sheet 1) and time per request (Sheet 2).
- ./ab.exe -n 100000 -c 100 http://localhost:8080/lines/8
- These results need to be investigated

What documentation, websites, papers, etc did you consult in doing this assignment?
- Books: Java Network Programming Fourth Edition, Elliotte Rusty Harold (http://a.co/d/520fKHj)
- Websites: Read a specific line from a file (https://stackoverflow.com/a/29637442/4086836)

What third-party libraries or other tools does the system use? How did you choose each library or framework you used?
I didn't use any third-party libraries.

How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?
- I spent about 5 hours on this assignment. If I had more time, I would address the optimisation and re-usability concerns. For the optimisation, two issues that I would consider first: to pre-process the file, and also to batch the number of user requests. Also, the code needs re-factoring and improve the error/exception handling. 

If you were to critique your code, what would you have to say about it?
The available implementation is minimal. The code needs to be re-factored to be able to handle new endpoints. 
