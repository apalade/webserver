package webserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author Andrei Palade <andrey.palade at googlemail.com>
 */
public class SingleFileHTTPServer {
    
    private static final int POOL_SIZE = 100;

    private final int port;
    
    private final String filePath;

    SingleFileHTTPServer(String path, int port) {
        this.filePath = path;
        this.port = port;
    }

    void start() {
        ExecutorService threadPool = Executors.newFixedThreadPool(POOL_SIZE);

        try (ServerSocket server = new ServerSocket(port)) {

            Logger.getLogger(SingleFileHTTPServer.class.getName()).log(Level.INFO, 
                    "Accepting connections on port {0}", server.getLocalPort());

            while (true) {
                Socket connection = server.accept();
                threadPool.submit(new HTTPRequestHandler(connection));
            }

        } catch (IOException ex) {
            Logger.getLogger(SingleFileHTTPServer.class.getName()).log(Level.SEVERE, 
                    "Could not start the server!");
        }
    }

    private class HTTPRequestHandler implements Callable<Void> {    

        private final Socket connection;

        public HTTPRequestHandler(Socket connection) {
            this.connection = connection;
        }

        @Override
        public Void call() throws Exception {
            try {
                OutputStream out = new BufferedOutputStream(connection.getOutputStream());
                InputStream in = new BufferedInputStream(connection.getInputStream());
                
                String request = readRequest(in);
                String response = "";
                
                if (isValid(request)) {
                    int lineIndex = 0; 
                    
                    if ((lineIndex = extractLineNumber(request)) != -1) {
                        
                        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
                            String lineData = lines.skip(lineIndex).findFirst().get();
                            response = createResponse(lineData);
                            
                        } catch (IllegalArgumentException | NullPointerException | NoSuchElementException ex) {
                            response = createBeyondEndOfFileResponse();
                        }
                    } 
                    else {
                        response = createBadRequestResponse();
                    }
                    
                } else {
                    response = createBadRequestResponse();
                }
                
                byte[] data = response.getBytes(Charset.forName("US-ASCII"));
                
                out.write(data);
                out.flush();
                
            } catch(IOException ex) {
                Logger.getLogger(SingleFileHTTPServer.class.getName()).log(Level.WARNING, null, ex);
                
            } finally {
                connection.close();
            }
            return null;
        }

        /**
         * Reads the first line received
         * 
         * @param in
         * @return The first line of the request
         * @throws IOException 
         */
        private String readRequest(InputStream in) throws IOException{
            
            // Reads the first line
            StringBuilder request = new StringBuilder(80);
            while (true) {
                int c = in.read();
                if (c == '\r' || c == '\n' || c == -1) {
                    break;
                }
                request.append((char) c);
            }
            return request.toString();
        }
        
        
        /**
         * Extracts the line index from the request
         * Assume the request string has format "GET /lines/<lineIndex> HTTP/1.1"
         * 
         * @param request 
         * @return Return file index if a valid URL, otherwise returns -1
         */
        private int extractLineNumber(String request) {
            int lineIndex = -1;
            try {
                String[] requestParts = request.split(" ");
                String[] lineParts = requestParts[1].split("/lines/");
                lineIndex = Integer.parseInt(lineParts[1]);
            } catch (Exception ex) {
                Logger.getLogger(SingleFileHTTPServer.class.getName()).log(Level.WARNING, "Invalid request");
            }
            return lineIndex;
        } 
        
        
        private String createResponse(String data) {
            
            String headerTemplate = "HTTP/1.1 200 OK\r\n"
                                + "Server: MyOneFileServer 1.0\r\n"
                                + "Content-length: " + data.length() + "\r\n"
                                + "Content-type: html text/html; charset=UTF-8\r\n\r\n";
            return headerTemplate + data;
        }
        
        private String createBeyondEndOfFileResponse() {
            
            String headerTemplate = "HTTP/1.1 413 Payload Too Large\r\n"
                                + "Server: MyOneFileServer 1.0\r\n"
                                + "Content-length: 0\r\n"
                                + "Content-type: html text/html; charset=UTF-8\r\n\r\n";
            return headerTemplate;
        }
        
        private String createBadRequestResponse () {
            
            String headerTemplate = "HTTP/1.1 400 Bad Request\r\n"
                                + "Server: MyOneFileServer 1.0\r\n"
                                + "Content-length: 0\r\n"
                                + "Content-type: html text/html; charset=UTF-8\r\n\r\n";
            return headerTemplate;
        }

        private boolean isValid(String request) {
            return request.length() > 0;
        }
    }
}
